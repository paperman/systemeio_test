<?php

namespace App\Controller;

use App\Form\OrderType;
use App\Service\Payment\PaymentService;
use App\Service\PriceService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    #[Route('/api/order', methods: ["POST"])]
    public function __invoke(Request $request, PriceService $priceService, PaymentService $paymentService): JsonResponse
    {
        $form = $this->createForm(OrderType::class);
        $form->submit($request->getPayload()->all());

        try {
            if ($form->isSubmitted() && $form->isValid()) {
                $price = $priceService->calculate($form->getNormData());
                return $this->json(['success' => $paymentService->process($form->getNormData()['paymentProcessor'], $price)]);
            }

            throw new \RuntimeException($form->getErrors(true, true)->current()->getMessage());
        } catch (\Throwable $e) {
            return $this->json(['message' => $e->getMessage()], 400);
        }
    }
}
