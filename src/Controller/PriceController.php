<?php

namespace App\Controller;

use App\Form\PriceType;
use App\Service\PriceService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PriceController extends AbstractController
{
    #[Route(path: "/api/price", methods: ['POST'])]
    public function __invoke(Request $request, PriceService $priceService)
    {
        $form = $this->createForm(PriceType::class);
        $form->submit($request->getPayload()->all());

        try {
            if ($form->isSubmitted() && $form->isValid()) {
                return $this->json(['price' => $priceService->calculate($form->getNormData())]);
            }

            throw new \RuntimeException($form->getErrors(true, true)->current()->getMessage());
        } catch (\Throwable $e) {
            return $this->json(['message' => $e->getMessage()], 400);
        }
    }
}
