<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('product', IntegerType::class, ['constraints' => new NotBlank(message: 'Product is required')])
            ->add('taxNumber', TextType::class, ['constraints' => new NotBlank(message: 'Tax number is required')])
            ->add('couponCode', TextType::class)
            ->add('paymentProcessor', TextType::class, ['constraints' => new NotBlank(message: 'Payment processor is required')]);
    }
}
