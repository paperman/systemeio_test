<?php

namespace App\Service;

use App\Repository\ProductRepository;
use App\Service\Tax\TaxService;

class PriceService
{
    public function __construct(
        private readonly ProductRepository $productRepository,
        private readonly DiscountService   $discountService,
        private readonly TaxService        $taxService
    )
    {
    }

    public function calculate(array $data): float
    {
        $product = $this->productRepository->findOrFail($data['product']);
        $priceWithDiscount = $this->discountService->getPriceWithDiscount($data['couponCode'], $product->getPrice());
        return $this->taxService->getPriceWithTax($data['taxNumber'], $priceWithDiscount);
    }
}
