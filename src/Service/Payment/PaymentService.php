<?php

namespace App\Service\Payment;

use App\Service\Payment\Handlers\PaymentHandlerInterface;

class PaymentService
{
    public function __construct(private readonly iterable $handlers)
    {
    }

    /**
     * @param string $processor
     * @param float $price
     * @return bool
     */
    public function process(string $processor, float $price): bool
    {
        /** @var PaymentHandlerInterface $handler */
        foreach ($this->handlers as $handler) {
            if ($handler->getAlias() === $processor) {
                return $handler->pay($price);
            }
        }

        throw new \RuntimeException('Payment processor not found');
    }
}
