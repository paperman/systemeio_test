<?php

namespace App\Service\Payment\Handlers;

use Systemeio\TestForCandidates\PaymentProcessor\StripePaymentProcessor;

class StripePaymentHandler implements PaymentHandlerInterface
{

    public function getAlias(): string
    {
        return 'stripe';
    }

    /**
     * @param float $price
     * @return bool
     */
    public function pay(float $price): bool
    {
        return (new StripePaymentProcessor())->processPayment(round($price * 100));
    }
}
