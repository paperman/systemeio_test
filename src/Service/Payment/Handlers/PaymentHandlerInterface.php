<?php

namespace App\Service\Payment\Handlers;

interface PaymentHandlerInterface
{
    public function getAlias(): string;

    public function pay(float $price): bool;
}
