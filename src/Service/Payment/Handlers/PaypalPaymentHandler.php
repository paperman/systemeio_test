<?php

namespace App\Service\Payment\Handlers;

use Systemeio\TestForCandidates\PaymentProcessor\PaypalPaymentProcessor;

class PaypalPaymentHandler implements PaymentHandlerInterface
{
    public function getAlias(): string
    {
        return 'paypal';
    }

    /**
     * @param float $price
     * @return bool
     */
    public function pay(float $price): bool
    {
        try {
            (new PaypalPaymentProcessor())->pay(round($price * 100));
            return true;
        } catch (\Exception) {
            return false;
        }
    }
}
