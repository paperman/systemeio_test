<?php

namespace App\Service;

use App\Entity\Coupon;
use App\Repository\CouponRepository;

class DiscountService
{
    public function __construct(
        private readonly CouponRepository $couponRepository
    )
    {
    }

    /**
     * @param string|null $couponCode
     * @param float $price
     * @return float
     */
    public function getPriceWithDiscount(?string $couponCode, float $price): float
    {
        if ($couponCode === null) {
            return $price;
        }

        $coupon = $this->couponRepository->findOrFail($couponCode);

        $result = match ($coupon->getType()) {
            Coupon::TYPE_FIXED => $price - ($coupon->getValue() ?? 0),
            Coupon::TYPE_PERCENT => $price * (1 - $coupon->getValue() / 100),
            default => throw new \RuntimeException('Unknown coupon type')
        };

        return round($result, 2);
    }
}
