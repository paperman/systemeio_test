<?php

namespace App\Service\Tax\Handlers;

abstract class AbstractTaxHandler implements TaxHandlerInterface
{
    public function support(string $taxNumber): bool
    {
        return mb_ereg_match($this->getPatten(), mb_strtolower($taxNumber));
    }

    abstract protected function getPatten(): string;

    public function calculate(float $price): float
    {
        return round($price * (1 + $this->getTaxValue() / 100), 2);
    }

    abstract protected function getTaxValue(): float;
}
