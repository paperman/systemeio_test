<?php

namespace App\Service\Tax\Handlers;

class ItalyTaxHandler extends AbstractTaxHandler
{

    protected function getPatten(): string
    {
        return '^it\d{11,11}$';
    }

    protected function getTaxValue(): float
    {
        return 22.0;
    }
}
