<?php

namespace App\Service\Tax\Handlers;

interface TaxHandlerInterface
{
    public function support(string $taxNumber): bool;

    public function calculate(float $price): float;
}
