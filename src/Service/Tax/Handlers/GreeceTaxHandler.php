<?php

namespace App\Service\Tax\Handlers;

class GreeceTaxHandler extends AbstractTaxHandler
{

    protected function getPatten(): string
    {
        return '^gr\d{9,9}$';
    }

    protected function getTaxValue(): float
    {
        return 24.0;
    }
}
