<?php

namespace App\Service\Tax\Handlers;

class FranceTaxHandler extends AbstractTaxHandler
{

    protected function getPatten(): string
    {
        return '^fr\w\w\d{9,9}$';
    }

    protected function getTaxValue(): float
    {
        return 20.0;
    }
}
