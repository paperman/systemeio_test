<?php

namespace App\Service\Tax\Handlers;

class GermanyTaxHandler extends AbstractTaxHandler
{

    protected function getPatten(): string
    {
        return '^de\d{9,9}$';
    }

    protected function getTaxValue(): float
    {
        return 19.0;
    }
}
