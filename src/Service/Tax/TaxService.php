<?php

namespace App\Service\Tax;

use App\Service\Tax\Handlers\TaxHandlerInterface;

class TaxService
{
    public function __construct(private readonly iterable $handlers)
    {
    }

    /**
     * @param string $taxNumber
     * @param float $price
     * @return float
     */
    public function getPriceWithTax(string $taxNumber, float $price): float
    {
        /** @var TaxHandlerInterface $handler */
        foreach ($this->handlers as $handler) {
            if ($handler->support($taxNumber)) {
                return $handler->calculate($price);
            }
        }

        throw new \RuntimeException('Unknown tax number');
    }

}
