<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230904221440 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('INSERT INTO product (name, price) VALUES (\'Iphone\', 100), (\'Наушники\', 20), (\'Чехол\', 10)');
        $this->addSql('INSERT INTO coupon (code, type, value) VALUES (\'D15\', \'percent\', 15), (\'D5\', \'fixed\', 5)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('TRUNCATE TABLE product');
        $this->addSql('TRUNCATE TABLE coupon');

    }
}
